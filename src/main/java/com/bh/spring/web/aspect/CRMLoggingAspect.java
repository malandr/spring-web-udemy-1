package com.bh.spring.web.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class CRMLoggingAspect {

    private Logger logger = Logger.getLogger(getClass().getName());

    @Pointcut("execution(* com.bh.spring.web.controller.*.*(..))")
    private void forControllerPackage() {}

    @Pointcut("execution(* com.bh.spring.web.service.*.*(..))")
    private void forServicePackage() {}

    @Pointcut("execution(* com.bh.spring.web.dao.*.*(..))")
    private void forDaoPackage() {}

    @Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
    private void forAppFlow() {}

    @Before("forAppFlow()")
    public void before(JoinPoint joinPoint) {

        String method = joinPoint.getSignature().toShortString();

        logger.info("\n =====> in @Before: calling method " + method);

        Object[] args = joinPoint.getArgs();

        for (Object o : args) {

            logger.info("===> Argument " + o);
        }
    }

    @AfterReturning(
            pointcut = "forAppFlow()",
            returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {

        String method = joinPoint.getSignature().toShortString();

        logger.info("\n =====> in @AfterReturning: from method " + method);

        logger.info("===> result: " + result.toString());
    }
}